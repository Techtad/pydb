import sys
sys.path.append("./modules")

from contractors import Contractors
from ui import UI

ui = UI()
db = Contractors()

def print_db():
    ui.inc_indent()
    print(db.to_string())
    input('Wciśnij dowolny klawisz...')
    menu()

def insert_records():
    ui.inc_indent()
    entry = {}
    for col in db.columns:
        value = ui.request_data(col)
        entry[col] = value
    db.insert(entry)
    ui.dec_indent()
    a = ui.get_answer('Dodawać kolejny rekord?(t/n) ', ['t', 'n', 'T', 'N'])
    if a.lower() == 't':
        insert_records()
    else:
        menu()

def delete_records():
    ui.inc_indent()
    a = ui.get_answer('Usunąć wg indeksu(I) czy wg wyszukiwania(W)?', ['i', 'w', 'I', 'W'])
    if a.lower() == 'i':
        i = ui.request_data_t("indeks", int)
        if db.delete_index(i):
            ui.print('Pomyślnie usunięto rekord')
        else:
            ui.print('Nie udało się usunąć rekordu')
    else:
        ui.print('Kolumny bazy danych: ' + str(db.columns))
        col = ui.get_answer('Wybierz kolumnę: ', db.columns)
        val = ui.request_data('szukaną wartość')
        delCount = db.delete_query(col, val)
        ui.print('Usunięto %i rekordów' % delCount)
    input('Wciśnij dowolny klawisz...')
    menu()

def search_records():
    ui.inc_indent()
    ui.print('Kolumny bazy danych: ' + str(db.columns))
    col = ui.get_answer('Wybierz kolumnę: ', db.columns)
    val = ui.request_data('szukaną wartość')
    result = db.search(col, val)
    ui.print(str(result))
    input('Wciśnij dowolny klawisz...')
    menu()

def load_db():
    ui.inc_indent()
    a = ui.request_data('nazwę pliku')
    db.load(a)
    menu()

def save_db():
    ui.inc_indent()
    a = ui.request_data('nazwę pliku')
    db.save(a)
    menu()

def menu():
    ui.set_indent(0)
    ui.print('Baza danych zawiera %i rekordów' % db.size())
    ui.print('W - Wypisanie danych')
    ui.print('D - Dodawanie rekordów')
    ui.print('U - Usuwanie rekordów')
    ui.print('S - Wyszukiwanie rekordów')
    ui.print('F - Ładowanie bazy z pliku')
    ui.print('Z - Zapisanie bazy w pliku')
    ui.print('Q - Wyjdź z programu')
    a = ui.get_answer('Wybierz(W/D/U/S/F/Z/Q): ', ['w', 'd', 'u', 's', 'f', 'z', 'q', 'W', 'D', 'U', 'S', 'F', 'Z', 'Q'])
    if a.lower() == 'w':
        print_db()
    elif a.lower() == 'd':
        insert_records()
    elif a.lower() == 'u':
        delete_records()
    elif a.lower() == 's':
        search_records()
    elif a.lower() == 'f':
        load_db()
    elif a.lower() == 'z':
        save_db()
    elif a.lower() == 'q':
        sys.exit()

menu()