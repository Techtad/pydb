class UI:
    def __init__(self):
        self.indentnum = 0
        self.indentstr = ''

    def set_indent(self, num):
        self.indentnum = num
        self.indentstr = ''
        for i in range(0, num):
            self.indentstr += '\t'
    
    def inc_indent(self):
        self.set_indent(self.indentnum + 1)
    
    def dec_indent(self):
        newIndent = self.indentnum - 1 if self.indentnum >= 1 else 0
        self.set_indent(newIndent)
    
    def print(self, msg):
        print(self.indentstr + msg)

    def request_data(self, name):
        data = input(self.indentstr + 'Podaj %s: ' % name)
        return data
    
    def get_answer(self, msg, valid_answers):
        anwser = ''
        while anwser not in valid_answers:
            anwser = input(self.indentstr + msg)
        return anwser

    def request_data_t(self, name, t):
        data = ''
        correct = False
        while not correct:
            try:
                data = t(input('Podaj %s (%s): ' % (name, str(t))))
                correct = True
            except ValueError:
                correct = False
        return data