from database import DataBase

class Contractors(DataBase):
    def __init__(self):
        super().__init__(['Firma', 'KodPocztowy', 'Miasto', 'Ulica', 'Kontakt', 'Email', 'Telefon'])
