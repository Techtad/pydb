import json

class DataBase:
    def __init__(self, columns):
        self.columns = columns
        self.entries = []

    def insert(self, values):
        entry = {}
        if type(values) == list or type(values) == tuple:
            for i in range(0, len(self.columns)):
                entry[self.columns[i]] = '-' if i >= len(values) else values[i]
        elif type(values) == dict:
            for col in self.columns:
                entry[col] = '-' if not col in values else values[col]
        self.entries.append(entry)

    def search(self, column, value):
        results = []
        for entry in self.entries:
            if entry[column] == value: results.append(entry)
        return results

    def delete(self, entry):
        if entry in self.entries:
            self.entries.remove(entry)
            return True
        else:
            return False
    
    def delete_index(self, index):
        if(index < len(self.entries)):
            self.entries.remove(self.entries[index])
            return True
        else:
            return False

    def delete_query(self, column, value):
        deletedCount = 0
        for entry in self.search(column, value):
            self.delete(entry)
            deletedCount += 1
        return deletedCount

    def to_string(self):
        s = '|\tIndex\t|'
        for col in self.columns:
            s += '\t' + col + '\t|'
        for i in range(0, len(self.entries)):
            row = '\n|\t' + str(i) + '\t|'
            entry = self.entries[i]
            for col in self.columns:
                row += '\t' + str(entry[col]) + '\t|'
            s += row
        return s
    
    def size(self):
        return len(self.entries)

    def save(self, filename):
        if len(filename) <= 5 or filename[-5:] != '.json': filename = '%s.json' % filename

        try:
            with open(filename, 'w') as file:
                json.dump(self.entries, file)
                return True
        except Exception:
            print("Błąd zapisywania do pliku: %s" % filename)
            return False

    def load(self, filename):
        if len(filename) <= 5 or filename[-5:] != '.json': filename = '%s.json' % filename

        try:
            with open(filename, 'r') as file:
                self.entries = json.load(file)
            return True
        except Exception:
                print("Błąd czytania pliku: %s" % filename)
                return False